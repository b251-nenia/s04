
# 1. Create an abstract class called Animal that has the following abstract methods:
	# a. eat(food)
	# b. make_sound()
from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass


# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
	# a. Properties:
		# i. Name
		# ii. Breed
		# iii. Age
	# b. Methods:
		# i. Getters
		# ii. Setter
		# iii. Implementation of abstract methods
		# iv. call()

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# Getter Methods
	def get_name(self):
		return self._name

	def get_breed(self):
		return self._breed

	def get_name(self):
		return self._age

	# Setter Method
	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed
	
	def set_age(self, age):
		self._age = age

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print(f"Woof! woof woof!!")

	def call(self):
		print(f"Here {self._name}!")

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# Getter Methods
	def get_name(self):
		return self._name

	def get_breed(self):
		return self._breed

	def get_name(self):
		return self._age

	# Setter Method
	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed
	
	def set_age(self, age):
		self._age = age

	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f"Miawwwccchiiieee!!")

	def call(self):
		print(f"{self._name}, come on!")


dog1 = Dog("Huggy", "Corgi", 2)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Missy", "Scottish Fold", 1)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

